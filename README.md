# NSO Sample Service

This repo is used to build a docker image of NSO containing a simple service used in CiscoLive DevNet Workshop (DEVWKS-3984)

## Folder structure

.
└── nso
    ├── config             - NSO conficuration files, such as authgroups.xml
    ├── ned-packages       - NEDs to be included in the image
    ├── nso-install-files  - NSO installation file
    ├── packages           - Service packagees to be included in the image
    └── scripts            - Additional scripts ot run pre-, post- or during container startup

## Image build procedure

1. Clone the repository
```
git clone https://gitlab.com/clemea23/nso-sample-service.git
```
2. Compile package
```
cd nso/packages/sample/src
make clean all
```
3. Download NSO installation binary into nso/nso-install-files folder
4. Download necessary NED installation files nso/into ned-packages folder
5. Build:

```
docker login registry.gitlab.com
docker build --build-arg PACKAGES_DIR=ned-packages --build-arg NSO_INSTALL_FILE=nso-install-files/nso-6.0.2.linux.x86_64.installer.bin -t registry.gitlab.com/clemea23/nso-sample-service nso/.
docker push registry.gitlab.com/clemea23/nso-sample-service
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/clemea23/nso-sample-service/-/settings/integrations)
